import random
from magic import *

conan_abilities = [bash, heavyblow, aimedstrike, overhandchop, annihilate]
grayfox_abilities = [heavyblow, aimedstrike, assassinate, shadowassault, vampirictouch]
melvin_abilities = [ice, lightning, earthquake, doom, vampirictouch, consumesoul, curemajor]
humbert_abilities = [zealousstrike, righteousmight, wordofpain, wordofsuffering, enervation, wordofpain, wordofdeath, curemajor]

warrior_abilities = [bash, heavyblow, aimedstrike, overhandchop]
rogue_abilities = [aimedstrike, lightning, shadowassault]
wizard_abilities = [magicmissile, frostbolt, fireball, arcanelance]
cleric_abilities = [zealousstrike, wordofpain, wordofsuffering, curemoderate]


class Person:
    def __init__(self, name, hp, energy, atk, armor, magicarmor, abilities, items, resistances):
        self.name = name
        self.maxhp = hp
        self.hp = hp
        self.maxenergy = energy
        self.energy = energy
        self.atkl = atk - 1
        self.atkh = atk + 1
        self.armor = armor
        self.magicarmor = magicarmor
        self.abilities = abilities
        self.items = items
        self.actions = ["Attack", "Abilities", "Items"]
        self.resistances = resistances



    def load_conan(self):
        # 10000 HP, 5000 Energy, 75-100 DMG, 50 Armor, 50 Magic Armor
        self.name = "Conan"
        self.maxhp = 10000
        self.hp = self.maxhp
        self.maxenergy = 5000
        self.energy = self.maxenergy
        self.atkl += 75
        self.atkh += 100
        self.armor = 50
        self.magicarmor = 50
        self.abilities.extend(conan_abilities)
        self.items[0]["quantity"] = 2
        self.items[1]["quantity"] = 2
        self.items[2]["quantity"] = 2
        self.items[3]["quantity"] = 2
        self.items[4]["quantity"] = 2
        self.items[5]["quantity"] = 2
        self.items[6]["quantity"] = 2
        self.items[7]["quantity"] = 2
        self.items[8]["quantity"] = 2
        self.items[9]["quantity"] = 2
        self.items[10]["quantity"] = 2
        self.resistances = [0, 20, 20, 20, 20, 20, 20, 20]

    def load_gray_fox(self):
        # 5000 HP, 10000 Energy, 50-100 DMG, 50 Armor, 50 Magic Armor
        self.name = "Gray Fox"
        self.maxhp = 5000
        self.hp = self.maxhp
        self.maxenergy = 100000
        self.energy = self.maxenergy
        self.atkl = self.atkl - 50
        self.atkh = self.atkh + 100
        self.armor = 50
        self.magicarmor = 50
        self.abilities.extend(grayfox_abilities)
        self.items[0]["quantity"] = 2
        self.items[1]["quantity"] = 2
        self.items[2]["quantity"] = 2
        self.items[3]["quantity"] = 2
        self.items[4]["quantity"] = 2
        self.items[5]["quantity"] = 2
        self.items[6]["quantity"] = 2
        self.items[7]["quantity"] = 2
        self.items[8]["quantity"] = 2
        self.items[9]["quantity"] = 2
        self.items[10]["quantity"] = 2
        self.resistances = [20, 20, 20, 20, 20, 20, 20, 20]

    def load_melvin(self):
        # 3000 HP, 10000 Energy, 25 DMG, 25 Armor, 100 Magic Armor
        self.name = "Melvin the Genius"
        self.maxhp = 3000
        self.hp = self.maxhp
        self.maxenergy = 10000
        self.energy = self.maxenergy
        self.atkl = self.atkl - 24
        self.atkh = self.atkh + 25
        self.armor = 25
        self.magicarmor = 100
        self.abilities.extend(melvin_abilities)
        self.items[0]["quantity"] = 2
        self.items[1]["quantity"] = 2
        self.items[2]["quantity"] = 2
        self.items[3]["quantity"] = 2
        self.items[4]["quantity"] = 2
        self.items[5]["quantity"] = 2
        self.items[6]["quantity"] = 2
        self.items[7]["quantity"] = 2
        self.items[8]["quantity"] = 2
        self.items[9]["quantity"] = 2
        self.items[10]["quantity"] = 2
        self.resistances = [20, 20, 20, 20, 20, 20, 20, 20]

    def load_humbert(self):
        # 3000 HP, 10000 Energy, 25 DMG, 25 Armor, 100 Magic Armor
        self.name = "Humbert the Pious"
        self.maxhp = 3000
        self.hp = self.maxhp
        self.maxenergy = 7000
        self.energy = self.maxenergy
        self.atkl = self.atkl - 24
        self.atkh = self.atkh + 25
        self.armor = 25
        self.magicarmor = 100
        self.abilities.extend(humbert_abilities)
        self.items[0]["quantity"] = 2
        self.items[1]["quantity"] = 2
        self.items[2]["quantity"] = 2
        self.items[3]["quantity"] = 2
        self.items[4]["quantity"] = 2
        self.items[5]["quantity"] = 2
        self.items[6]["quantity"] = 2
        self.items[7]["quantity"] = 2
        self.items[8]["quantity"] = 2
        self.items[9]["quantity"] = 2
        self.items[10]["quantity"] = 2
        self.resistances = [20, 20, 20, 20, 20, 20, 20, 20]

    def create_new_warrior(self):
        # 5000 HP, 2500 Energy, 50 DMG, 50 Armor, 10 Magic Armor
        print("You have chosen WARRIOR.")
        self.maxhp = 5000
        self.hp = self.maxhp
        print("Warriors have", str(self.maxhp), "health.")

        self.maxenergy = 2500
        self.energy = self.maxenergy
        print("Warriors have", str(self.maxenergy), "energy.")

        self.atkl = self.atkl - 49
        self.atkh = self.atkh + 51
        print("The default Warrior damage span is ±50.")

        self.armor = 50
        self.magicarmor = 10
        print("Warriors have", str(self.armor), "Defense, and", str(self.magicarmor), "Magic Defense.")

        self.abilities.extend(warrior_abilities)
        print("Warriors begin play with the following abilities:")
        for i in range(len(warrior_abilities)):
            print(i+1, "-", warrior_abilities[i].name)
        # Shows abilities in warrior_abilities, not player_abilities because of inherent ones to be removed later

        print("Warriors begin play with the following items:")
        self.items[0]["quantity"] = 2
        self.items[2]["quantity"] = 1
        self.items[9]["quantity"] = 1
        for item in self.items:
            if item["quantity"] == 1:
                print("x" + str(item["quantity"]), item["item"].name)
            if item["quantity"] >= 2:
                print("x" + str(item["quantity"]), item["item"].name + "s")

        # resistances = [physical, fire, frost, earth, air, arcane, holy, unholy]
        self.resistances = [20, 5, 5, 5, 5, 5, 5, 5]

    def create_new_rogue(self):
        # 3000 HP, 3000 Energy, 25 DMG, 25 Armor, 25 Magic Armor
        print("You have chosen ROGUE.")
        self.maxhp = 3000
        self.hp = self.maxhp
        print("Rogues have", str(self.maxhp), "health.")

        self.maxenergy = 3000
        self.energy = self.maxenergy
        print("Rogues have", str(self.maxenergy), "energy.")

        self.atkl = self.atkl - 25
        self.atkh = self.atkh + 25
        print("The default rogue damage span is ±25.")

        self.armor = 25
        self.magicarmor = 25
        print("Rogues have", str(self.armor), "Defense, and", str(self.magicarmor), "Magic Defense.")

        self.abilities.extend(rogue_abilities)
        print("Rogues begin play with the following abilities:")
        for i in range(len(rogue_abilities)):
            print(i+1, "-", rogue_abilities[i].name)
        # Shows abilities in rogue_abilities, not player_abilities because of inherent ones to be removed later

        print("Rogues begin play with the following items:")
        self.items[0]["quantity"] = 0
        self.items[1]["quantity"] = 1
        self.items[2]["quantity"] = 0
        self.items[3]["quantity"] = 0
        self.items[4]["quantity"] = 1
        self.items[5]["quantity"] = 0
        self.items[6]["quantity"] = 0
        self.items[7]["quantity"] = 0
        self.items[8]["quantity"] = 2
        self.items[9]["quantity"] = 0
        self.items[10]["quantity"] = 0
        for item in self.items:
            if item["quantity"] == 1:
                print("x" + str(item["quantity"]), item["item"].name)
            if item["quantity"] >= 2:
                print("x" + str(item["quantity"]), item["item"].name + "s")
        # resistances = [physical, fire, frost, earth, air, arcane, holy, unholy]
        self.resistances = [10, 10, 5, 5, 5, 5, 5, 15]

    def create_new_wizard(self):
        # 2500 HP, 5000 Energy, 10 DMG, 10 Armor, 50 Magic Armor
        print("You have chosen WIZARD.")
        self.maxhp = 2500
        self.hp = self.maxhp
        print("Wizards have", str(self.maxhp), "health.")
        self.maxenergy = 5000
        self.energy = self.maxenergy
        print("Wizards have", str(self.maxenergy), "energy.")
        self.atkl = self.atkl - 10
        self.atkh = self.atkh + 10
        print("The default wizard damage span is ±10.")

        self.armor = 10
        self.magicarmor = 50
        print("Wizards have", str(self.armor), "Defense, and", str(self.magicarmor), "Magic Defense.")

        self.abilities.extend(wizard_abilities)
        print("Wizards begin play with the following abilities:")
        for i in range(len(wizard_abilities)):
            print(i+1, "-", wizard_abilities[i].name)
        # Shows abilities in wizard_abilities, not player_abilities because of inherent ones to be removed later

        print("Wizards begin play with the following items:")
        self.items[0]["quantity"] = 0
        self.items[1]["quantity"] = 1
        self.items[2]["quantity"] = 0
        self.items[3]["quantity"] = 2
        self.items[4]["quantity"] = 0
        self.items[5]["quantity"] = 0
        self.items[6]["quantity"] = 0
        self.items[7]["quantity"] = 1
        self.items[8]["quantity"] = 0
        self.items[9]["quantity"] = 0
        self.items[10]["quantity"] = 0
        for item in self.items:
            if item["quantity"] == 1:
                print("x" + str(item["quantity"]), item["item"].name)
            if item["quantity"] >= 2:
                print("x" + str(item["quantity"]), item["item"].name + "s")
        # resistances = [physical, fire, frost, earth, air, arcane, holy, unholy]
        self.resistances = [5, 10, 10, 10, 10, 25, 10, 10]

    def create_new_cleric(self):
        # 3000 HP, 5000 Energy, 10 DMG, 10 Armor, 25 Magic Armor
        print("You have chosen CLERIC.")
        self.maxhp = 3000
        self.hp = self.maxhp
        print("Clerics have", str(self.maxhp), "health.")
        self.maxenergy = 5000
        self.energy = self.maxenergy
        print("Clerics have", str(self.maxenergy), "energy.")
        self.atkl = self.atkl - 10
        self.atkh = self.atkh + 10
        print("The default cleric damage span is ±10.")

        self.armor = 10
        self.magicarmor = 50
        print("Clerics have", str(self.armor), "Defense, and", str(self.magicarmor), "Magic Defense.")

        self.abilities.extend(cleric_abilities)
        print("Clerics begin play with the following abilities:")
        for i in range(len(cleric_abilities)):
            print(i+1, "-", cleric_abilities[i].name)
        # Shows abilities in cleric_abilities, not player_abilities because of inherent ones to be removed later

        print("Clerics begin play with the following items:")
        self.items[0]["quantity"] = 0
        self.items[1]["quantity"] = 1
        self.items[2]["quantity"] = 0
        self.items[3]["quantity"] = 0
        self.items[4]["quantity"] = 1
        self.items[5]["quantity"] = 0
        self.items[6]["quantity"] = 0
        self.items[7]["quantity"] = 1
        self.items[8]["quantity"] = 0
        self.items[9]["quantity"] = 0
        self.items[10]["quantity"] = 10
        for item in self.items:
            if item["quantity"] == 1:
                print("x" + str(item["quantity"]), item["item"].name)
            if item["quantity"] >= 2:
                print("x" + str(item["quantity"]), item["item"].name + "s")
        # resistances = [physical, fire, frost, earth, air, arcane, holy, unholy]
        self.resistances = [10, 5, 5, 5, 5, 5, 50, 25]

    def generate_damage(self):
        return random.randrange(self.atkl, self.atkh)

        # resistances = [physical, fire, frost, earth, air, arcane, holy, unholy]
    def player_resistance_calculation(self, action_school, dmg):
        resistance_type = 0

        if action_school == "physical":
            resistance_type = self.resistances[0]
        elif action_school == "fire":
            resistance_type = self.resistances[1]
        elif action_school == "frost":
            resistance_type = self.resistances[2]
        elif action_school == "earth":
            resistance_type = self.resistances[3]
        elif action_school == "air":
            resistance_type = self.resistances[4]
        elif action_school == "arcane":
            resistance_type = self.resistances[5]
        elif action_school == "holy":
            resistance_type = self.resistances[6]
        elif action_school == "unholy":
            resistance_type = self.resistances[7]

        if action_school == "noschool":
            print("no school")
            return dmg
        else:
            res_reduction = int((resistance_type * dmg) / 100)
            if res_reduction > 0:
                dmg -= res_reduction
                if dmg <= 0:
                    dmg = 1
                print("Your", action_school.capitalize(), "resistance lowered the damage by", str(res_reduction) + "!")
            return dmg

    def enemy_resistance_calculation(self, action_school, dmg):
        resistance_type = 0

        if action_school == "physical":
            resistance_type = self.resistances[0]
        elif action_school == "fire":
            resistance_type = self.resistances[1]
        elif action_school == "frost":
            resistance_type = self.resistances[2]
        elif action_school == "earth":
            resistance_type = self.resistances[3]
        elif action_school == "air":
            resistance_type = self.resistances[4]
        elif action_school == "arcane":
            resistance_type = self.resistances[5]
        elif action_school == "holy":
            resistance_type = self.resistances[6]
        elif action_school == "unholy":
            resistance_type = self.resistances[7]

        if action_school == "noschool":
            return dmg
        else:
            res_reduction = int((resistance_type * dmg) / 100)
            if res_reduction > 0:
                dmg -= res_reduction
                if dmg <= 0:
                    dmg = 1
                print(self.name + "'s", action_school.capitalize(), "resistance lowered the damage by", str(res_reduction) + "!")
            return dmg

    def player_take_physical_damage(self, dmg, action_school):
        try:
            dmg = self.player_resistance_calculation(action_school, dmg)

            dmg = dmg - self.armor
            if dmg < 1:
                dmg = 1
            print("Your Physical Armor reduced the damage to", str(dmg) + "!")

            self.hp -= dmg
            if self.hp < 0:
                self.hp = 0
            print("New HP:", str(self.hp) + "/" + (str(self.maxhp)))
            return self.hp
        except TypeError:
            print("TYPE ERROR player take physical damage")

    def player_take_magic_damage(self, dmg, action_school):
        try:
            dmg = self.player_resistance_calculation(action_school, dmg)

            dmg = dmg - self.magicarmor
            if dmg < 1:
                dmg = 1
            print("Your Magic Armor reduced the damage to", str(dmg) + "!")

            self.hp -= dmg
            if self.hp < 0:
                self.hp = 0
            print("New HP:", str(self.hp) + "/" + (str(self.maxhp)))
            return self.hp
        except TypeError:
            print("TYPE ERROR player take magic damage")

    def enemy_take_physical_damage(self, dmg, name, action_school):
        try:
            print()
            print("You attack", name + "!")
            print(self.name + "'s HP Is:", self.hp)

            dmg = self.enemy_resistance_calculation(action_school, dmg)

            dmg = dmg - self.armor
            if dmg < 1:
                dmg = 1
            print(self.name + "'s Physical Armor lowered the damage to", str(dmg) + "!")
            self.hp -= dmg
            if self.hp < 0:
                self.hp = 0
            return self.hp
        except TypeError:
            print("TYPE ERROR enemy take physical damage")

    def enemy_take_magic_damage(self, dmg, name, action_school):
        try:
            print()
            print("You attack", name + "!")
            print(self.name + "'s HP Is:", self.hp)

            dmg = self.enemy_resistance_calculation(action_school, dmg)

            dmg = dmg - self.magicarmor
            if dmg < 1:
                dmg = 1
            print("But", self.name + "'s Magic Armor lowered the damage to", str(dmg) + "!")
            self.hp -= dmg
            if self.hp < 0:
                self.hp = 0
            return self.hp
        except TypeError:
            print("TYPE ERROR enemy take magic damage")

    def heal(self, value):
        self.hp += value
        if self.hp > self.maxhp:
            self.hp = self.maxhp

    def restore(self, value):
        self.energy += value
        if self.energy > self.maxenergy:
            self.energy = self.maxenergy

    def get_hp(self):
        return self.hp

    def get_maxhp(self):
        return self.maxhp

    def get_energy(self):
        return self.energy

    def get_maxenergy(self):
        return self.maxenergy

    def reduce_energy(self, cost):
        self.energy -= cost

    def choose_action(self, actions):
        print("Choose Action:")
        try:
            for i, choice in enumerate(actions):
                print(str(i+1) + ".", choice)
            choice = input()
            if not 0 < int(choice) <= len(actions):
                input("\n" + str(choice) + " doesn't correspond to any actions.\nPress *Enter* to retry:")
                return False
            else:
                return int(choice)
        except ValueError:
            input("\nDigits only.\nPress *Enter* to retry:")
            return False

    def choose_ability(self, abilities):
        while True:
            print("Choose Ability:")
            try:
                for i, choice in enumerate(abilities):
                    print(str(i+1)+ ".", choice.name, "(cost:", str(choice.cost) + ")")
                choice = int(input())-1
                if (choice < 0) or (choice >= len(abilities)):
                    print("\n" + str(choice+1) + " doesn't correspond to any abilities.\nPress *Enter* to retry:")
                    print("--- Clearing UI ---" + "\n" * 5)
                    continue
                else:
                    return int(choice)
            except ValueError:
                input("\nDigits only.\nPress *Enter* to retry:\n--- Clearing UI ---" + "\n" * 5)
                continue

    def choose_item(self):
        print("\n" + "ITEMS")
        while True:
            i = 0
            try:
                for item in self.items:
                    print(str(i+1) + ".",item["item"].name, ":", item["item"].description, "(x" + str(item["quantity"]) + ")")
                    i += 1
                item_choice = int(input("\nChoose item: ")) - 1
                if item_choice <= -1 or item_choice >= len(self.items):
                    print("That doesn't correspond to any items in your inventory.")
                    print("\n--- Clearing UI ---" + "\n" * 5)
                    continue
                if self.items[item_choice]["quantity"] == 0:
                    print("You don't have any more", self.items[item_choice]["item"].name + "s!")
                    print("\n--- Clearing UI ---" + "\n" * 5)
                    continue
                elif self.items[item_choice]["quantity"] >= 1:
                    self.items[item_choice]["quantity"] -= 1
                    return self.items[item_choice]["item"]
            except ValueError:
                input("\nDigits only.\nPress *Enter* to retry:\n--- Clearing UI ---" + "\n" * 5)
                continue

    def choose_target(self, enemies):
        while True:
            i = 1
            print("\n", "TARGET:")
            try:
                for enemy in enemies:
                    if enemy.get_hp() != 0:
                        print("\t" + str(i) + ".", enemy.name)
                        i += 1
                choice = int(input("Choose target:"))-1
                if (choice < 0) or (choice >= len(enemies)):
                    print("\n" + str(choice+1), "doesn't correspond to any targets.\nPress *Enter* to retry:")
                    print("--- Clearing UI ---" + "\n" * 5)
                    continue
                else:
                    return choice
            except ValueError:
                input("\nDigits only.\nPress *Enter* to retry:\n--- Clearing UI ---" + "\n" * 5)
                continue

    def enemy_turn(self):
        while True:
            print()
            enemy_choice = self.random_enemy_choice()

            if enemy_choice == 0:
                enemy_dmg = self.generate_damage()
                print(self.name, "attacks you for", enemy_dmg, "damage!")
                return enemy_dmg, "attack", "noschool"

            elif enemy_choice == 1:
                ability = self.enemy_choose_ability()
                if not ability:
                    # print(self.name, "does not have enough energy for another spell!")
                    continue
                else:
                    ability_dmg = self.enemy_ability_damage(ability)
                    self.reduce_energy(ability.cost)

                    if ability.abilitytype == "healing":
                        self.heal(ability_dmg)
                        print(self.name, "casts", ability.name, "and is healed for", str(ability_dmg), "hp!")
                        return 0, "healing", ability.magicschool

                    elif ability.abilitytype == "drain":
                        print(self.name, "attacks you with", ability.name, "for", str(ability_dmg), "damage!")
                        self.heal(ability_dmg)
                        print(self.name, "heals for", str(ability_dmg), "with", ability.name + "!")
                        return ability_dmg, "drain", ability.magicschool

                    elif ability.abilitytype == "magic":
                        print(self.name, "attacks you with", ability.name, "for", str(ability_dmg), "damage!")
                        return ability_dmg, "magic", ability.magicschool

                    elif ability.abilitytype == "physical":
                        print(self.name, "attacks you with", ability.name, "for", str(ability_dmg), "damage!")
                        return ability_dmg, "physical", ability.magicschool

            elif enemy_choice == 2:
                item_choice = random.randrange(0, len(self.items))

                item = self.items[item_choice]["item"]
                hp_percentage = self.hp / self.maxhp * 100
                energy_percentage = self.energy / self.maxenergy * 100

                if self.items[item_choice]["quantity"] == 0:
                    # Tries to use item but has no more. Goes to next iteration of enemy_turn
                    continue
                else:
                    if item.type == "healthpotion":
                        if hp_percentage > 33:
                            # Too much HP to use a healthpotion
                            continue
                        else:
                            self.heal(item.prop)
                            print(self.name, "heals self for", item.prop, "with the", item.name + "!")
                            self.items[item_choice]["quantity"] -= 1
                            return 0, "healthpotion", "noschool"

                    if item.type == "energypotion":
                        if energy_percentage > 33:
                            # Too much energy to use an energypotion
                            continue
                        else:
                            self.restore(item.prop)
                            print(self.name, "restores", item.prop, "energy with the", item.name + "!")
                            self.items[item_choice]["quantity"] -= 1
                            return 0, "energypotion", "noschool"

                    elif item.type == "elixir":
                        if hp_percentage > 20:
                            continue
                        else:
                            self.hp = self.maxhp
                            self.energy = self.maxenergy
                            print(self.name, "has been fully restored with the", item.name + "!")
                            self.items[item_choice]["quantity"] -= 1
                            return 0, "elixir", "noschool"

                    elif item.type == "attack":
                        print(self.name, "brandishes a", item.name + "!")
                        self.items[item_choice]["quantity"] -= 1
                        return item.prop, "item", "noschool"

            print("LOOOOOOOOP BREAK")
            break

    def enemy_choose_ability(self):
        i = 0
        while i < len(self.abilities):
            hp_percentage = self.hp / self.maxhp * 100
            # ability_choice = random.randrange(0, len(self.abilities))
            # random_ability = self.abilities[ability_choice]
            # These lines do nothing. Enemies do not select spells randomly.
            for ability in self.abilities:
                if self.energy < ability.cost:
                    # print(self.name, "does not have enough energy for", ability.name)
                    i += 1
                    continue
                else:
                    if ability.abilitytype == "healing" and hp_percentage > 33:
                        # print(self.name, "has too much HP to use", ability.name)
                        continue
                    return ability
            break

    def enemy_ability_damage(self, ability):
        ability_dmg = ability.dmg
        return ability_dmg

    def random_enemy_choice(self):
        random_choice = random.randrange(0, 3)
        return random_choice

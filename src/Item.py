class Item:
    def __init__(self, name, type, description, prop):
        self.name = name
        self.type = type
        self.description = description
        self.prop = prop


# HP Potions
potion = Item("Potion", "healthpotion", "Heals 50 HP", 50)
highpotion = Item("Hi-Potion", "healthpotion", "Heals for 100 HP", 100)
superpotion = Item("Super Potion", "healthpotion", "Heals for 500 HP", 500)

# Energy Potions
energydrink = Item("Energy Drink", "energypotion", "Heals 50 HP", 50)
manapotion = Item("Mana Potion", "energypotion", "Heals for 100 HP", 100)
supermanapotion = Item("Super Mana Potion", "energypotion", "Heals for 500 HP", 500)

# Elixirs
elixir = Item("Elixir", "elixir", "Fully restores HP & MP of one individual", 9999)
megaelixir = Item("Mega-elixir", "elixir", "Fully restores HP & MP of all party members", 9999)

# ITEMS Damage
molotov = Item("Molotov Cocktail", "attack", "Deals 100 damage", 100)
grenade = Item("Grenade", "attack", "Deals 250 damage", 250)
holygrenade = Item("Holy Handgrenade", "attack", "Deals 1000 damage", 1000)

player_items = [{"item": potion, "quantity": 0},
                {"item": highpotion, "quantity": 0},
                {"item": superpotion, "quantity": 0},
                {"item": energydrink, "quantity": 0},
                {"item": manapotion, "quantity": 0},
                {"item": supermanapotion, "quantity": 0},
                {"item": elixir, "quantity": 0},
                {"item": megaelixir, "quantity": 0},
                {"item": molotov, "quantity": 0},
                {"item": grenade, "quantity": 0},
                {"item": holygrenade, "quantity": 0}]

no_items = [{"item": potion, "quantity": 0},
            {"item": highpotion, "quantity": 0},
            {"item": superpotion, "quantity": 0},
            {"item": elixir, "quantity": 0},
            {"item": megaelixir, "quantity": 0},
            {"item": molotov, "quantity": 0},
            {"item": grenade, "quantity": 0},
            {"item": holygrenade, "quantity": 0}]

enemy_items = [{"item": potion, "quantity": 1},
               {"item": molotov, "quantity": 1}]

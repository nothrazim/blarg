# import os
import time
from Person import Person
from magic import *
from Item import *


# MENU STARTS
run_state = True

# MAP GEN #
map_corner = None
map_size = None
character_selected = None

player_abilities = [firebolt, curelesser]
bigguy_abilities = [curemoderate, thunderstrike, stonespike, frostbolt]
smallpeople_abilities = [bash, curelesser]


player_resistances = [10, 11, 12, 13, 14, 15, 16, 17]
bigguy_resistances = [10, 20, 30, 40, 50, 60, 70, 80]
smallpeople_resistances = [10, 11, 12, 13, 14, 15, 16, 17]
# Creature stats:
# Name, HP, Energy, ATK, armor, Magic armor, Abilities, Items, Resistances
# Creatures
player = Person("Playername", 100, 100, 10, 10, 10, player_abilities, player_items, player_resistances)

enemy = Person("Big Guy", 1500, 500, 30, 25, 25, bigguy_abilities, enemy_items, bigguy_resistances)
enemy2 = Person("Small Guy", 250, 500, 20, 20, 20, smallpeople_abilities, enemy_items, smallpeople_resistances)
enemy3 = Person("Small Fry", 250, 500, 20, 20, 20, smallpeople_abilities, enemy_items, smallpeople_resistances)
enemies = [enemy, enemy2, enemy3]

def menu_start():
    print(player.__dict__)
    print("### WELCOME! ###")
    option = validate_strings("Login", "Sign up", "Quit")
    if not option:
        menu_start()
    elif option == 1:
        menu_login()
    elif option == 2:
        menu_sign_up()
    elif option == 3:
        return False
    else:
        print("DEBUG: Running else, not good!")


def menu_login():
    print("### LOGIN ###")
    account_name = input("Account name: ")
    # account_password = input("Password: ")
    # pw isn't used right now anyway
    clear()
    print("Login successful!\nUsername:" + account_name)
    sleep()
    clear()
    menu_characters()


def menu_sign_up():
    print("### SIGN UP ###")
    account_name = input("Account name: ")
    account_password = input("Password: ")
    clear()
    print(account_name, account_password, "\nAccount created!")
    print("Welcome newbie!")
    menu_characters_new()


def menu_characters():
    print("### CHARACTERS OPTIONS ###")
    options = validate_strings("Load existing", "Create new", "Return to Start")
    if not options:
        menu_characters()
    if options == 1:
        menu_characters_load()
    if options == 2:
        menu_characters_new()
    if options == 3:
        menu_start()
    else:
        print("DEBUG: In the else, not good!")


def menu_characters_load():
    print("### CHARACTER SELECT ###")
    character_list = ['Conan', 'Gray Fox', 'Melvin the Genius', 'Humbert the Pious']
    options = validate_dictionary(character_list)
    if options == 1:
        player.load_conan()
    if options == 2:
        player.load_gray_fox()
    if options == 3:
        player.load_melvin()
    if options == 4:
        player.load_humbert()
    if not options:
        menu_characters_load()
    global character_selected
    character_selected = character_list[options-1]
    print("You've selected character:", character_selected)
    sleep()
    menu_game_options()


def menu_characters_new():
    print("### New character creation ###")
    print("### You must select a NAME! ###")
    player.name = input("Enter name:")
    print("### Class ###")
    class_choice = validate_strings("Warrior", "Rogue", "Wizard", "Cleric")
    if not class_choice:
        menu_characters_new()
    if class_choice == 1:
        player.create_new_warrior()

    if class_choice == 2:
        player.create_new_rogue()

    if class_choice == 3:
        player.create_new_wizard()

    if class_choice == 4:
        player.create_new_cleric()

    print("You've created a hero by the name of: ", player.name)
    global character_selected
    character_selected = player.name
    sleep()
    menu_game_options()


def menu_game_options():
    clear()
    print("### GAME DETAILS ###")
    global map_size, map_corner
    print("Character Creation:  [X]", character_selected)
    if not map_size:
        print("Map Size:            [ ]")
    else:
        print("Map Size:            [X]", map_size)
    if not map_corner:
        print("Map Corner:          [ ]")
    else:
        print("Map Corner:          [X]", map_corner)
    print("-" * 15)
    option = validate_strings("Map Size", "Starting corner", "Start game already!", "Return to Character Menu")
    if option == 1:
        menu_map_size()
    elif option == 2:
        menu_map_corner()
    elif option == 3:
        dungeon_start(map_size, map_corner)
    elif option == 4:
        menu_characters()
    menu_game_options()


def menu_map_size():
    while True:
        clear()
        print("### MAP SIZE SELECT ###")
        global map_size
        option = validate_strings("3 x 3", "4 x 4", "5 x 5", "Return")
        if not option:
            continue
        if option == 1:
            map_size = "3 x 3"
        elif option == 2:
            map_size = "4 x 4"
        elif option == 3:
            map_size = "5 x 5"
        elif option == 5:
            return False
        return True


def menu_map_corner():
    while True:
        clear()
        print("### MAP CORNER SELECT ###")
        global map_corner
        option = validate_strings("North West", "North East", "South West", "South East", "Return")
        if not option:
            continue
        elif option == 1:
            map_corner = "North West"
        elif option == 2:
            map_corner = "North East"
        elif option == 3:
            map_corner = "South West"
        elif option == 4:
            map_corner = "South East"
        elif option == 5:
            return False
        return True


def dungeon_start(map_size, map_corner):
    print("### READY? ###")
    print("Character:", character_selected)
    print("Size:", map_size)
    print("Corner:", map_corner)
    input("Press *ENTER* to start!")
    clear()
    game_combat()


def game_combat():
    turn_count = 0
    turn_count +=1
    running = True
    while running:
        clear()
        print("TURN COUNT:", turn_count)
        # Test if battle is over
        defeated_enemies = 0
        for enemy in enemies:
            if enemy.get_hp() == 0:
                defeated_enemies += 1
        if defeated_enemies == len(enemies):
            print("You win!")
            break

        if player.get_hp() == 0:
            print("You have died...")
            break

        # Print HP & Energy for player and every living enemy
        for enemy in enemies:
            print(enemy.name + " HP:", str(enemy.get_hp()) + "/" + str(enemy.get_maxhp()))
            print(enemy.name + " Energy:", str(enemy.get_energy()) + "/" + str(enemy.get_maxenergy()))
        print(player.name, "HP:", str(player.get_hp()) + "/" + str(player.get_maxhp()))
        print(player.name, "Energy:", str(player.get_energy()) + "/" + str(player.get_maxenergy()))
        print("-" * 15)

        input("Press ENTER to confirm: ")
        clear()

        # choice = validate_strings("Attack", "Abilities", "Items")
        choice = player.choose_action(player.actions)
        index = int(choice) - 1
        clear()

        # ATTACK!
        if index == 0:
            dmg = player.generate_damage()
            enemy = player.choose_target(enemies)

            action_school = "physical"

            enemies[enemy].enemy_take_physical_damage(dmg, enemies[enemy].name, action_school)
            if dmg-enemies[enemy].armor < 1:
                print("You've hit " + enemies[enemy].name + " for 1 damage!")
            else:
                print("You've hit " + enemies[enemy].name + " for " + str(dmg-enemies[enemy].armor) + " damage!")
            # Moved "you've hit X" here since abilities and items should not generate the output.
            # Maybe all of this should be it's own attack function (especially if we add weapons that
            # modifies the damage output)

            if enemies[enemy].get_hp() == 0:
                print(enemies[enemy].name + " has died!")
                del enemies[enemy]

        # Abilities
        elif index == 1:
            ability_choice = player.choose_ability(player_abilities)
            clear()

            ability = player.abilities[ability_choice]
            ability_dmg = ability.generate_ability_damage()
            current_energy = player.get_energy()
            action_school = ability.magicschool

            if ability.cost > current_energy:
                input("This ability requires too much energy \nPress *ENTER* to confirm.")
                continue
            player.reduce_energy(ability.cost)

            if ability.abilitytype == "healing":
                player.heal(ability_dmg)
                print(ability.name, "heals you for", str(ability_dmg), "HP.")

            elif ability.abilitytype == "drain":
                enemy = player.choose_target(enemies)
                # no validate to avoid index out of range!

                enemies[enemy].enemy_take_magic_damage(ability.dmg, enemies[enemy].name, action_school)
                final_damage = ability_dmg - enemies[enemy].magicarmor
                print("Your", ability.name, "deals", str(final_damage), "damage to", enemies[enemy].name + "!")

                player.heal(final_damage)
                print(ability.name + " heals you for", str(final_damage), "HP.")

                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name + " has died!")
                    del enemies[enemy]

            elif ability.abilitytype == "magic":
                enemy = player.choose_target(enemies)
                final_damage = ability_dmg - enemies[enemy].magicarmor
                # no validate to avoid index out of range!

                enemies[enemy].enemy_take_magic_damage(ability_dmg, enemies[enemy].name, action_school)
                print("Your", ability.name, "deals", str(final_damage), "damage to", enemies[enemy].name + "!")

                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name + " has died!")
                    del enemies[enemy]

            elif ability.abilitytype == "physical":
                enemy = player.choose_target(enemies)
                final_damage = ability_dmg - enemies[enemy].armor
                # no validate to avoid index out of range!

                enemies[enemy].enemy_take_physical_damage(ability_dmg, enemies[enemy].name, action_school)
                print("Your", ability.name, "deals", str(final_damage), "damage to", enemies[enemy].name + "!")

                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name + " has died!")
                    del enemies[enemy]
        # Items
        elif index == 2:
            item_choice = player.choose_item()
            action_school = "physical"
            clear()

            if item_choice.type == "healthpotion":
                player.heal(item_choice.prop)
                print("Your", item_choice.name, "heals you for", item_choice.prop, "HP.")

            if item_choice.type == "energypotion":
                player.restore(item_choice.prop)
                print("Your", item_choice.name, "restores", item_choice.prop, "Energy.")

            elif item_choice.type == "elixir":
                player.hp = player.maxhp
                player.energy = player.maxenergy
                print("Your", item_choice.name, "has fully restored y1ou!")

            elif item_choice.type == "attack":
                enemy = player.choose_target(enemies)
                enemies[enemy].enemy_take_physical_damage(item_choice.prop, enemies[enemy].name, action_school)
                final_damage = item_choice.prop - enemies[enemy].armor

                print("Your", item_choice.name, "deals", str(final_damage), "damage to " + enemies[enemy].name + "!")

                if enemies[enemy].get_hp() == 0:
                    print(enemies[enemy].name + " has died!")
                    del enemies[enemy]
        elif index > 2 or index < 0:
            print("Select an action.")
            continue

        while True:
            for enemy in enemies:
                enemy_dmg, enemy_action_type, action_school = enemy.enemy_turn()
                if enemy_action_type == "attack" or enemy_action_type == "item":
                    print("enemy action type: attack/item")
                    player.player_take_physical_damage(enemy_dmg, action_school)
                elif enemy_action_type == "magic" or enemy_action_type == "drain":
                    player.player_take_magic_damage(enemy_dmg, action_school)
                elif enemy_action_type == "physical":
                    print("enemy action type: physical")
                    player.player_take_physical_damage(enemy_dmg, action_school)
                elif enemy_action_type == "healing" or enemy_action_type == "healthpotion":
                    pass
                elif enemy_action_type == "elixir" or enemy_action_type == "energypotion":
                    pass
                else:
                    print("None of above")
                    continue
            turn_count += 1
            break


def validate_strings(*menu_choices):
    try:
        for i in range(len(menu_choices)):
            print(i+1, "-", menu_choices[i])
        choice = input()
        if not 0 < int(choice) <= len(menu_choices):
            input("\n" + str(choice) + " doesn't correspond to any menu number.\nPress *Enter* to retry:")
            clear()
            return False
        else:
            return int(choice)
    except ValueError:
        input("\nDigits only.\nPress *Enter* to retry:")
        clear()
        return False


def validate_dictionary(menu_choices):
    try:
        for i, choice in enumerate(menu_choices):
            print(i+1, "-", choice)
        choice = input()
        if not 0 < int(choice) <= len(menu_choices):
            input("\n" + str(choice) + " doesn't correspond to any menu number.\nPress *Enter* to retry:")
            return False
        else:
            return int(choice)
    except ValueError:
        input("\nDigits only.\nPress *Enter* to retry:")
        return False
    finally:
        clear()


def sleep():
    time.sleep(0.5)


def clear():
    print("--- Clearing UI ---" + "\n" * 5)
    # os.system(cls)


while run_state:
    run_state = menu_start()
    print("Good bye!")

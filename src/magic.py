import random


class Ability:
    def __init__(self, name, cost, dmg, abilitytype, magicschool):
        self.name = name
        self.cost = cost
        self.dmg = dmg
        self.abilitytype = abilitytype
        self.magicschool = magicschool

    def generate_ability_damage(self):
        low = self.dmg - 15
        high = self.dmg + 15
        return random.randrange(low, high)


# # # # # # #
# ABILITIES #
# # # # # # #
# Physical
bash = Ability("Bash", 100, 100, "physical", "physical")
heavyblow = Ability("Heavy Blow", 200, 200, "physical", "physical")
aimedstrike = Ability("Aimed Strike", 300, 300, "physical", "physical")
overhandchop = Ability("Overhand Chop", 400, 400, "physical", "physical")
whirlwind = Ability("Whirlwind", 400, 400, "physical", "physical")
steeldance = Ability("Steel Dance", 400, 400, "physical", "physical")
assassinate = Ability("Assassinate", 500, 500, "physical", "physical")
annihilate = Ability("Annihilate", 500, 500, "physical", "physical")

zealousstrike = Ability("Zealous Strike", 250, 250, "physical", "holy")
righteousmight = Ability("Righteous Might", 300, 300, "physical", "holy")
fistheavens = Ability("Fist of the Heavens", 500, 500, "physical", "holy")

arcanespear = Ability("Arcane Spear", 500, 500, "physical", "arcane")


# Spells
magicmissile = Ability("Magic Missile", 100, 100, "magic", "arcane")
arcaneblast = Ability("Arcane Blast", 200, 200, "magic", "arcane")
arcanelance = Ability("Arcane Lance", 300, 300, "magic", "arcane")
arcanebarrage = Ability("Arcane Barrage", 400, 400, "magic", "arcane")
arcaneorb = Ability("Arcane Orb", 500, 500, "magic", "arcane")

wordofpain = Ability("Word of Pain", 100, 100, "magic", "holy")
wordofsuffering = Ability("Word of Suffering", 200, 200, "magic", "holy")
wordofpower = Ability("Word of Power", 400, 400, "magic", "holy")
wordofdeath = Ability("Word of Death", 500, 500, "magic", "holy")

lightning = Ability("Lightning", 200, 200, "magic", "air")
electricorb = Ability("Electric Orb", 300, 300, "magic", "air")
thunderstrike = Ability("Thunderstrike", 400, 400, "magic", "air")
thunderstorm = Ability("Thunderstorm", 500, 500, "magic", "air")

tremors = Ability("Tremors", 100, 100, "magic", "earth")
stonespike = Ability("Stone Spike", 200, 200, "magic", "earth")
earthquake = Ability("Earthquake", 500, 500, "magic", "earth")

enervation = Ability("Enervation", 300, 300, "magic", "unholy")
fingerofdeath = Ability("Finger of Death", 400, 400, "magic", "unholy")
doom = Ability("DOOOOM", 500, 500, "magic", "unholy")

shadowassault = Ability("Shadow Assault", 200, 200, "magic", "unholy")

frostbolt = Ability("Frostbolt", 100, 100, "magic", "frost")
ice = Ability("Ice", 200, 200, "magic", "frost")
blizzard = Ability("Blizzard", 200, 300, "magic", "frost")
icestorm = Ability("Icestorm", 400, 400, "magic", "frost")
absolutezero = Ability("Absolute Zero", 500, 500, "magic", "frost")

firebolt = Ability("Firebolt", 100, 100, "magic", "fire")
flamingsphere = Ability("Flaming Sphere", 300, 300, "magic", "fire")
fireball = Ability("Fireball", 300, 300, "magic", "fire")
firestorm = Ability("Firestorm", 400, 400, "magic", "fire")
meteorshower = Ability("Meteor Shower", 500, 500, "magic", "fire")

# Drain
vampirictouch = Ability("Vampiric Touch", 100, 100, "drain", "unholy")
drainlife = Ability("Drain Life", 300, 300, "drain", "unholy")
consumesoul = Ability("Consume Soul", 400, 400, "drain", "unholy")

# Healing
curelesser = Ability("Cure Lesser Wounds", 100, 100, "healing", "holy")
curemoderate = Ability("Cure Wounds", 200, 200, "healing", "holy")
curemajor = Ability("Cure Major Wounds", 300, 300, "healing", "holy")
